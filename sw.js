const staticCacheName = "tasks-static-v2";
const dynamicCacheName = "tasks-dynamic-v2";
const assets = [
  "/",
  "/index.html",
  "/js/app.js",
  "/js/ui.js",
  "/js/materialize.min.js",
  "/css/style.css",
  "/css/materialize.min.css",
  "/img/tasks.svg",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://fonts.gstatic.com/s/materialicons/v47/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2",
  "/pages/fallback.html",
];

// install event
self.addEventListener("install", (evt) => {
  //console.log('service worker installed');
  evt.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      cache.addAll(assets);
    })
  );
});

// activate event
self.addEventListener("activate", (evt) => {
  //console.log('service worker activated');
  evt.waitUntil(
    caches.keys().then((keys) => {
      //console.log(keys);
      return Promise.all(
        keys
          .filter((key) => key !== staticCacheName && key !== dynamicCacheName)
          .map((key) => caches.delete(key))
      );
    })
  );
});

// fetch event
self.addEventListener("fetch", (evt) => {
  if (
    evt.request.url.indexOf("firestore.googleapis.com") === -1 &&
    evt.request.url.indexOf(
      "https://navinkarkera.pythonanywhere.com/test-push-notification"
    ) === -1
  ) {
    evt.respondWith(
      caches
        .match(evt.request)
        .then((cacheRes) => {
          return (
            cacheRes ||
            fetch(evt.request).then((fetchRes) => {
              return caches.open(dynamicCacheName).then((cache) => {
                cache.put(evt.request.url, fetchRes.clone());
                return fetchRes;
              });
            })
          );
        })
        .catch(() => {
          if (evt.request.url.indexOf(".html") > -1) {
            return caches.match("/pages/fallback.html");
          }
        })
    );
  }
});

const dispnotfunction = (notificationData) => {
  if (Notification.permission == "granted") {
    const dt = new Date();
    const dtstr = dt.toLocaleDateString();
    const tstr = dt.toLocaleTimeString();

    var options = {
      body: notificationData.body,
      dir: "auto",
      tag: dt.toISOString(),
      icon: "/img/icons/maskable_icon.png",
      timestamp: dt.toISOString(),
      vibrate: [100, 50, 100],
      data: {
        id: dt.toISOString(),
      },
      actions: [
        {
          action: "explore",
          title: "Explore task",
        },
        {
          action: "close",
          title: "Close notification",
        },
      ],
    };
    self.registration.showNotification(notificationData.title, options);
  } else {
    alert("You need to allow permission to receive Push Notifications.");
  }
};

self.addEventListener("push", function (event) {
  const promiseChain = dispnotfunction(event.data.json().notification);

  event.waitUntil(promiseChain);
});

self.addEventListener("notificationclick", function (e) {
  var notification = e.notification;
  var action = e.action;

  if (action === "close") {
    notification.close();
  } else {
    const urlToOpen = new URL("/", self.location.origin).href;

    const promiseChain = clients
      .matchAll({
        type: "window",
        includeUncontrolled: true,
      })
      .then((windowClients) => {
        let matchingClient = null;

        for (let i = 0; i < windowClients.length; i++) {
          const windowClient = windowClients[i];
          if (windowClient.url === urlToOpen) {
            matchingClient = windowClient;
            break;
          }
        }

        if (matchingClient) {
          return matchingClient.focus();
        } else {
          return clients.openWindow(urlToOpen);
        }
      });

    e.waitUntil(promiseChain);
    notification.close();
  }
});
