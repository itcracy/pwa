// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyDwd74x3RM_awGtSlDOBPjQhA7v2h7B4lE",
  authDomain: "itcracy-tasks.firebaseapp.com",
  databaseURL: "https://itcracy-tasks.firebaseio.com",
  projectId: "itcracy-tasks",
  storageBucket: "itcracy-tasks.appspot.com",
  messagingSenderId: "1014094608778",
  appId: "1:1014094608778:web:38ff646ff306b6adfcf073",
  measurementId: "G-S2Q4RJR48E",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const messaging = firebase.messaging();

messaging.usePublicVapidKey(
  "BOff0bKDh0D7kuV3v5wtyr8XyIEJSYebjQHyvccKW0gPGNFtrdO8zwv97mvPaxmLkgkrTDHqIuoHcjU6p8fO3Ao"
);

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("/sw.js")
    .then((reg) => {
      console.log("service worker registered");
      messaging.useServiceWorker(reg);
      console.log("using existing worker as messaging worker");
    })
    .catch((err) => console.log("service worker not registered", err));
}
const subbtn = document.querySelector(".subscribe");
if (subbtn !== null) {
  subbtn.onclick = () => {
    const permission = Notification.permission;
    if (permission !== "granted") {
      Notification.requestPermission().catch((err) => {
        console.log(err);
      });
    } else {
      alert("You have already allowed to recive notifications.");
    }
  };
}

messaging.onMessage((msgPayload) => {
  console.log("onMessage", msgPayload);
  alert(msgPayload);
});
