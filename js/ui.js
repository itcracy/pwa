const tasks = document.querySelector(".tasks");

document.addEventListener("DOMContentLoaded", function () {
  var btnf = document.querySelectorAll(".btn-floating");
  var instances = M.FloatingActionButton.init(btnf);

  // colllipsable in form
  var collap = document.querySelectorAll(".collapsible");
  var instances = M.Collapsible.init(collap);

  // nav menu
  const menus = document.querySelectorAll(".side-menu");
  M.Sidenav.init(menus, { edge: "right" });
  // add task form
  const forms = document.querySelectorAll(".side-form");
  M.Sidenav.init(forms, { edge: "left" });

  // initialize the form date and time picker
  var dtel = document.querySelectorAll(".form-datepicker");
  var dtinst = M.Datepicker.init(dtel, { format: "dd/mm/yyyy" });
  var tel = document.querySelectorAll(".form-timepicker");
  var timeinst = M.Timepicker.init(tel, { twelveHour: false });
});

// render task
const renderTask = (data, id) => {
  if (tasks !== null) {
    const dt = new Date(data.reminder.seconds * 1000);
    const dtstr = dt.toLocaleDateString();
    const tstr = dt.toLocaleTimeString();

    const html = `

    <div class="row task" data-id="${id}">
      <div class="col s12">
        <div class="card hoverable medium" >
          <div class="card-image">
            <img src="/img/tasks.svg" width="139px" height="139px" />
          </div>
          <div class="card-fab">
            <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons" data-id="${id}">delete</i></a>
          </div>
          <div class="card-content grey-text text-darken-4 center-align">
            <span class="card-title">${data.title}</span>
          </div>
          <div class="card-action grey-text text-darken-1">
          <div class="row">
            <div class="input-field col s12 m4 offset-m4">
              <i class="material-icons prefix">event</i>
              <label>${dtstr}</label>
            </div>
            <div class="input-field col s12 m4 offset-m4">
              <i class="material-icons prefix">alarm</i>
              <label>${tstr}</label>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 center-align ">
              <p>
              ${data.description}
              </p>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

  `;

    tasks.innerHTML += html;
  }
};

// remove task from DOM
const removeTask = (id) => {
  const task = document.querySelector(`.task[data-id="${id}"]`);
  task.remove();
};

document.querySelector("#push-btn").onclick = () => {
  console.log("Clicked");
  messaging
    .requestPermission()
    .then(() => {
      console.log("Notification Permission");
      return messaging.getToken();
    })
    .then((token) => {
      var theUrl = `https://navinkarkera.pythonanywhere.com/test-push-notification/${token}`;
      fetch(theUrl, { mode: "no-cors" }).catch((err) => {
        console.log(err);
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
