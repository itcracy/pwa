// enable offline data
db.enablePersistence().catch(function (err) {
  if (err.code == "failed-precondition") {
    // probably multible tabs open at once
    console.log("persistance failed");
  } else if (err.code == "unimplemented") {
    // lack of browser support for the feature
    console.log("persistance not available");
  }
});

// real-time listener
db.collection("tasks").onSnapshot((snapshot) => {
  snapshot.docChanges().forEach((change) => {
    if (change.type === "added") {
      renderTask(change.doc.data(), change.doc.id);
    }
    if (change.type === "removed") {
      // remove the document data from the web page
      removeTask(change.doc.id);
    }
  });
});
//
// add new task
const form = document.querySelector("form");
if (form !== null) {
  form.addEventListener("submit", (evt) => {
    evt.preventDefault();
    var dtsplit = form.date.value.split("/");
    var tsplit = form.time.value.split(":");
    var timestamp = new Date(
      Number(dtsplit[2]),
      Number(dtsplit[1]) - 1,
      Number(dtsplit[0]),
      Number(tsplit[0]),
      Number(tsplit[1]),
      0
    );

    const task = {
      title: form.title.value,
      description: form.description.value,
      reminder: timestamp,
    };

    db.collection("tasks")
      .add(task)
      .catch((err) => console.log(err));

    form.title.value = "";
    form.description.value = "";
    form.date.value = "";
    form.time.value = "";
  });
}
//
// remove a task
const taskContainer = document.querySelector(".tasks");
if (taskContainer !== null) {
  taskContainer.addEventListener("click", (evt) => {
    if (evt.target.tagName === "I") {
      const id = evt.target.getAttribute("data-id");
      //console.log(id);
      db.collection("tasks").doc(id).delete();
    }
  });
}
